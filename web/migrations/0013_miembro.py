# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0012_auto_20160226_2353'),
    ]

    operations = [
        migrations.CreateModel(
            name='Miembro',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100)),
                ('area', models.CharField(max_length=100)),
                ('descripcion', ckeditor.fields.RichTextField(verbose_name='Contenido de la noticia')),
            ],
        ),
    ]
