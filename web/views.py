from django.http import Http404
from django.template import RequestContext, loader
from django.http import HttpResponse
from web.models import Entrada, Area, Evento, Actividad, Miembro
from itertools import chain
from operator import attrgetter

def index(request):
    #lista_ultimas_entradas = Entrada.objects.order_by('-fecha')[:4]
    lista_ultimas_entradas = Entrada.objects.order_by('-fecha')[:10]
    lista_ultimas_cultural = Actividad.objects.order_by('-fecha')[:10]
    areas = Area.objects.all()
    template = loader.get_template('noticias/index.html')
    result_list = reversed(
        sorted(
            chain(lista_ultimas_entradas, lista_ultimas_cultural),
            key=attrgetter('fecha')))
    context = RequestContext(request, {
        'user': request.user,
        'lista_ultimas_entradas': result_list,
        'areas': areas,
    })
    return HttpResponse(template.render(context))

def about(request):
    areas = Area.objects.all()
    miembros = Miembro.objects.all()
    template = loader.get_template('noticias/about.html')
    context = RequestContext(request, {
        'user': request.user,
        'miembros': miembros,
        'areas': areas,
    })
    return HttpResponse(template.render(context))

def contact_view(request):
    template = loader.get_template('noticias/contacts.html')
    areas = Area.objects.all()
    context = RequestContext(request, {
        'user': request.user,
        'areas': areas,
    })
    return HttpResponse(template.render(context))

def faq_view(request):
    template = loader.get_template('noticias/preguntas_frecuentes.html')
    areas = Area.objects.all()
    context = RequestContext(request, {
        'user': request.user,
        'areas': areas,
    })
    return HttpResponse(template.render(context))

def politica_privacidad(request):
    # TODO
    template = loader.get_template('noticias/politica_privacidad.html')
    context = RequestContext(request, {'user': request.user,})
    return HttpResponse(template.render(context))

def entrada(request, entrada_id):
    try:
        entrada = Entrada.objects.get(id=entrada_id)
    except Entrada.DoesNotExist:
        raise Http404("La entrada no existe")
    template = loader.get_template('noticias/noticia.html')
    context = RequestContext(request, {
        'user': request.user,
        'entrada': entrada,
    })
    return HttpResponse(template.render(context))

def area(request, area):
    try:
        areaObject = Area.objects.get(nombre=area)
    except Area.DoesNotExist:
        raise Http404("El area no existe")
    try:
        entradas = Entrada.objects.filter(id_area=areaObject.id).order_by('-fecha')
    except Entrada.DoesNotExist:
        raise Http404("La entrada no existe")

    template = loader.get_template('noticias/area.html')
    context = RequestContext(request, {
        'user': request.user,
        'area': areaObject,
        'entradas': entradas
    })
    return HttpResponse(template.render(context))

def area_cultural(request):
    area = "Cultural"

    eventos = Evento.objects.all()

    template = loader.get_template('noticias/area_cultural.html')
    context = RequestContext(request, {
        'user': request.user,
        'eventos': eventos,
    })
    return HttpResponse(template.render(context))

def actividad(request, actividad_id):
    try:
        actividad = Actividad.objects.get(id=actividad_id)
    except Actividad.DoesNotExist:
        raise Http404("El evento no existe")
    template = loader.get_template('noticias/actividad.html')
    context = RequestContext(request, {
        'user': request.user,
        'entrada': actividad,
    })
    return HttpResponse(template.render(context))