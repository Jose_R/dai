from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponsePermanentRedirect
from web.models import Area, Evento, Actividad
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404


@login_required
def perfil_view(request):
	user_form = UserForm(request.POST, instance=request.user)
	if request.method == 'POST':

		if user_form.is_valid():
			print(request.user)
			user = request.user
			user.email = user_form.cleaned_data['email']
			user.save()
			context = {
			}
			return render(request, 'accounts/index.html', context)
	else:  # get
		user_form = UserForm(instance=request.user)
	context = {'user': request.user,
	           'user_form': user_form}
	return render(request, 'accounts/perfil.html', context)


def registro_usuario_view(request):
	user_form = RegistroUserForm(request.POST)
	userProfile_form = PerfilForm(request.POST, request.FILES)
	if request.method == 'POST':

		# Comprobamos si el formulario es valido
		if user_form.is_valid() and userProfile_form.is_valid():
			user = user_form.save()
			profile = userProfile_form.save(commit=False)
			profile.user = user
			profile.save()


	# Creamos el contexto
	context = {'user_form': user_form,
	           'userProfile_form': userProfile_form
	           }
	# Y mostramos los datos
	return render(request, 'accounts/registro.html', context)


@login_required
def gracias_view(request, username):
	return render(request, 'accounts/gracias.html', {'user': request.user,
	                                                 'username': username})


@login_required
def index_view(request):
	template = 'accounts/index.html'
	context ={
		'user': request.user,
	}
	return render(request, template, context)


@login_required
def panel_view(request):
	return render(request, 'accounts/panel.html')


@staff_member_required
@login_required
def elegirEntrada_view(request):
	areas = Area.objects.all()
	template = 'accounts/elegirArea.html'
	context = {
		'user': request.user,
		'areas': areas,
	}
	return render(request, template, context)

@staff_member_required
@login_required
def modificarEntrada_view(request):
	areas = Area.objects.all()
	template = 'accounts/elegirAreaEntrada.html'
	context = {
		'user': request.user,
		'areas': areas,
	}
	return render(request, template, context)


## TODO imagenes, redireccion
@login_required
def entrada_view(request, area):
	# template = loader.get_template('accounts/entradasAdmin.html')
	area_obj = Area.objects.get(nombre=area)
	user = User.objects.get(username=request.user)
	tittle = "Creando entrada"
	action = "Crear"
	if request.method == 'POST':
		# Si el method es post, obtenemos los datos del formulario
		form = EntradaNuevaForm(request.POST, request.user)

		# Comprobamos si el formulario es valido
		if form.is_valid():
			post = form.save(commit=False)
			post.id_area_id = area_obj.id
			post.id_usuario = user
			post.imagen = "a"
			post.save()

			# url = reverse('web.entradas')

			return redirect('/entradas/')
	else:
		# Si el mthod es GET, instanciamos un objeto RegistroUserForm vacio
		form = EntradaNuevaForm()
	# Creamos el contexto
	context = {'form': form,
	           'tittle': tittle,
	           'action': action}
	# Y mostramos los datos
	return render(request, 'accounts/entradasAdmin.html', context)
	# return HttpResponse(template.render(context))


def login_view(request):
	# Si el usuario esta ya logueado, lo redireccionamos a index_view
	if request.user.is_authenticated:
		return redirect(reverse('accounts:accounts.index'))

	mensaje = ''
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				login(request, user)
				return redirect(reverse('accounts.index'))
			else:
				# Redireccionar informando que la cuenta esta inactiva
				# TODO
				pass
		mensaje = 'Nombre de usuario o contraseña no valido'
	return render(request, 'accounts/login.html', {'mensaje': mensaje})


def logout_view(request):
	logout(request)
	# messages.success(request, 'Te has desconectado con exito.')
	return redirect(reverse('accounts.login'))


@staff_member_required
@login_required
def areasAdmin_view(request):
	areas = Area.objects.all()
	template = loader.get_template('accounts/areasAdmin.html')
	context = RequestContext(request, {
		'user': request.user,
		'areas': areas,
	})
	return HttpResponse(template.render(context))


@staff_member_required
@login_required
def entradasAdmin_view(request):
	context = {'user': request.user}
	return render(request, 'accounts/entradasAdmin.html',context)


@staff_member_required
@login_required
def createArea_view(request):
	if request.method == 'POST':
		# Si el method es post, obtenemos los datos del formulario
		form = AreaForm(request.POST)

		# Comprobamos si el formulario es valido
		if form.is_valid():
			cleaned_data = form.cleaned_data
			nombre_data = cleaned_data.get('nombre')
			area_model = Area()
			area_model.nombre = nombre_data
			area_model.save()
			# Ahora, redireccionamos a la pagina accounts/gracias.html
			# Pero lo hacemos con un redirect.
			return redirect(reverse('accounts:accounts.areasAdmin'))
	else:
		# Si el mthod es GET, instanciamos un objeto RegistroUserForm vacio
		form = AreaForm()
	# Creamos el contexto
	context = {'form': form}
	# Y mostramos los datos
	return render(request, 'accounts/createArea.html', context)


@staff_member_required
@login_required
def createActividad_view(request, act):
	try:
		evento = Evento.objects.get_or_create(titulo=act)
	except Evento.DoesNotExist:
		if act == 'Eventos' or act == 'Entrega de premios' or act == 'Actividades':
			print("Creando %s" % act)
			print("Imposible pasar por aqui...")
			#Evento.objects.create(act)
	# Arriba lo crea, pero no lo acaba de devolver bien...
	evento = Evento.objects.get(titulo=act)

	user = User.objects.get(username=request.user)
	if request.method == 'POST':
		# Si el method es post, obtenemos los datos del formulario
		form = ActividadForm(request.POST)

		# Comprobamos si el formulario es valido
		if form.is_valid():
			print("-----------------------------------------------------------------------------------------")
			print(evento)
			print(type(evento))
			post = form.save(commit=False)
			post.id_evento_id = evento.id
			post.id_usuario = user
			post.save()
			# Ahora, redireccionamos a la pagina accounts/gracias.html
			# Pero lo hacemos con un redirect.
			return redirect(reverse('accounts:accounts.areasAdmin'))
	else:
		form = ActividadForm()
	# Creamos el contexto
	context = {'form': form,
	           'name': act}
	# Y mostramos los datos
	return render(request, 'accounts/createActividad.html', context)


@staff_member_required
@login_required
def entradasArea_view(request, area):
	try:
		areaObject = Area.objects.get(nombre=area)
	except Area.DoesNotExist:
		raise Http404("El area no existe")
	try:
		entradas = Entrada.objects.filter(id_area=areaObject.id).order_by('-fecha')
	except Entrada.DoesNotExist:
		raise Http404("La entrada no existe")

	template = 'accounts/elegirEntradaArea.html'
	context = {
		'area': areaObject,
		'entradas': entradas
	}
	return render(request, template, context)


@staff_member_required
@login_required
def updateEntrada_view(request, entrada_id):
	entrada = Entrada.objects.get(id=entrada_id)
	if request.method == 'POST':
		context = RequestContext(request, {
			'user': request.user,
		})
		form = EntradaNuevaForm(request.POST or None, instance=entrada)
		if 'delete' in request.POST:
			entrada.delete()
			return redirect(reverse('accounts:accounts.index'), context)
		elif form.is_valid():
			form.save()
			print("Hola")
			return redirect(reverse('accounts:accounts.index'))

	action = "Modificar"
	tittle = "Modificando entrada"
	delete = True
	form = EntradaNuevaForm(instance=entrada)
	template = loader.get_template('accounts/entradasAdmin.html')
	context = {
		'user': request.user,
		'tittle': tittle,
		'action': action,
		'form': form,
		'delete': delete,
	}

	return render(request, 'accounts/entradasAdmin.html', context)


@staff_member_required
@login_required
def updateActividad_view(request, entrada_id):
	entrada = Actividad.objects.get(id=entrada_id)
	if request.method == 'POST':
		form = ActividadNuevaForm(request.POST or None, instance=entrada)
		if 'delete' in request.POST:
			entrada.delete()
			return redirect(reverse('accounts:accounts.index'))
		elif form.is_valid():
			form.save()

			return redirect(reverse('accounts:accounts.index'))

	action = "Modificar"
	tittle = "Modificando entrada"
	delete = True
	form = ActividadNuevaForm(instance=entrada)
	template = 'accounts/entradasAdmin.html'
	context = {
		'tittle': tittle,
		'action': action,
		'form': form,
		'delete': delete,
	}
	return render(request, template, context)


def url_soloUPV(request):
	return HttpResponsePermanentRedirect("accounts/soloUPV.html")


# def error404(request):
#    return HttpResponsePermanentRedirect("index")

def error_view(request, error):
	return render(request, 'accounts/error.html', {'error': error})
	# return redirect(reverse('accounts.error', kwargs={'error': error}))


@staff_member_required
@login_required
def anadirMiembro_view(request):
	if request.method == 'POST':
		# Si el method es post, obtenemos los datos del formulario
		form = AnadirMiembroForm(request.POST)

		# Comprobamos si el formulario es valido
		if form.is_valid():
			post = form.save(commit=False)
			##post.id_evento_id = evento.id
			##post.id_usuario = user
			post.save()
			# Ahora, redireccionamos a la pagina accounts/gracias.html
			# Pero lo hacemos con un redirect.
			return redirect(reverse('accounts:accounts.panel'))
	else:
		form = AnadirMiembroForm()
	# Creamos el contexto
	action = "Añadir"
	tittle = "Añadiendo miembro de delegación"
	template = 'accounts/anadirMiembro.html'
	context = {
		'tittle': tittle,
		'action': action,
		'form': form,
	}
	# Y mostramos los datos
	return render(request,template, context)


@staff_member_required
@login_required
def miembros_view(request):
	miembros = Miembro.objects.all()

	template = 'accounts/miembros.html'
	context = {
		'miembros': miembros,
	}
	return render(request,template ,context)


@staff_member_required
@login_required
def modificarMiembro_view(request, id):
	miembro = Miembro.objects.get(id=id)
	if request.method == 'POST':
		form = AnadirMiembroForm(request.POST or None, instance=miembro)
		if 'delete' in request.POST:
			miembro.delete()
			return redirect(reverse('accounts:accounts.miembros'))
		elif form.is_valid():
			form.save()

			return redirect(reverse('accounts:accounts.miembros'))

	action = "Modificar"
	tittle = "Modificando miembro"
	form = AnadirMiembroForm(instance=miembro)
	template = 'accounts/anadirMiembro.html'
	context ={
		'tittle': tittle,
		'action': action,
		'form': form,
	}
	return render(request, template, context)
