{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Hola desde la pagina web de la Delegacion de Alumnos de Informatica!

Estas recibiendo este email para activar el usuario {{ user_display }} en nuestra web.

Para confirmar, dirigete a {{ activate_url }}
{% endblocktrans %}{% endautoescape %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}Gracias desde la DAI!
{{ site_domain }}{% endblocktrans %}
